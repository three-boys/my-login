var express = require('express');
var querystring= require('querystring');
var ObjectID = require('mongodb').ObjectID;
var db=require('../Common/CRUD/article').CRUD;
var tag=require('../Common/CRUD/tag').CRUD;
var comm=require('../Common/CRUD/comments').CRUD;

var router = express.Router();

//获取所有文章
//当前页码（pageindex）
//每页显示数量（pagesize）
//搜索标识（st{1：标题搜索，2：类型搜索}）
//搜索条件（txtsearch）
router.get("/getArticles",function(req,res){
	var pageindex = req.query.pageIndex;
	var pagesize = req.query.pageSize;
	var txtsearch = req.query.txtsearch==undefined?"":req.query.txtsearch;
	var st=req.query.st;
	var selectstat= st==1?"Type":"Title";
	var query={};
	var fields={"_id":1,"Title":1,"Type":1,"CreateDate":1,"like":1,"comments":1};
	if(!txtsearch=="")
	{
		query[selectstat]=new RegExp(txtsearch);//模糊查询参数
	}
	db.read(query,fields,pagesize,pageindex,function(result){
		res.end(JSON.stringify(result));
	})
});

//根据ID获取详细
//文章ID（id）
router.get('/getDetails', function (req, res) {
	var id = req.query.Id;
	var query={"_id":ObjectID(id)}
	var fields={"_id":1,"Title":1,"Type":1,"CreateDate":1,"like":1,"comments":1,"Content":1,"nolike":1};
	db.readdetails(query,fields,function(result){
		res.end(JSON.stringify(result));
	})
});

//获取评论
router.get('/getComment',function(req,res){
	var id=req.query.Id;
	var pageindex = req.query.pageIndex;
	var pagesize = req.query.pageSize;
	var query={"articleId":id};
	var fields={"_id":0,"content":1};

	comm.read(query,fields,pagesize,pageindex,function(result){
		res.end(JSON.stringify(result));
	})
})

//获取所有类型
router.get('/getTags', function (req, res) {
	tag.read({},function(result){
		res.end(JSON.stringify(result));
	})
});

//评论
router.post('/insertMessage',function(req,res){
	var jsons = JSON.parse(Object.keys(req.body));
	var articleId=jsons.articleId;
	var content=jsons.contents;
	var model={"articleId":articleId,"content":content}
	comm.create(model,function(result){
		res.end(JSON.stringify(result));
	})
})

//点赞
router.get('/like',function(req,res){
	var id=req.query.Id;
	var query={"_id":ObjectID(id)}
	var update={"$inc":{"like":1}}
	db.update(query,update,function(result){
		res.end(JSON.stringify(result));
	})
})
//踩
router.get('/nolike',function(req,res){
	var id=req.query.Id;
	var query={"_id":ObjectID(id)}
	var update={"$inc":{"nolike":1}}
	db.update(query,update,function(result){
		res.end(JSON.stringify(result));
	})
})

// router.get('/test123', function (req, res) {
//     var contents = req.query.contents;
// 	res.end(JSON.stringify(contents));
// });

module.exports = router;