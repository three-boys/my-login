var express = require('express');
var query = require('querystring');
var usr = require('dao/dbConnect');
var url = require('url');
var util = require("util");

var sd = require('silly-datetime'); //时间

var jsonresult = require('message/JsonResult'); //json数据封装
var router = express.Router();


//插入日志
router.get('/insertlogs', function (req, res) {
      var ip = req.connection.remoteAddress;
      var operation = req.query.operation
      var articleId = req.query.articleId;
      var Createtime = sd.format(new Date(), 'YYYY-MM-DD HH:mm');
      var sql = "insert into web_log(dt,Ip,operation,articleId) values('" + Createtime + "','" + ip + "','" + operation + "'," + articleId + ")";
      client = usr.connect();
      usr.insertFun(client, sql, function (result) {
            res.end(JSON.stringify(jsonresult.message(true, "成功！")));
      })
})

//获取日志信息
// router.get('/getlogs', function (req, res) {
//       var sqlcounts = 0;
//       var sqlonedays = 0;
//       var sqldetailss = 0;
//       var sqlarticlecounts = 0;
//       var sqlcount = "SELECT count(DISTINCT Ip) count from web_log;";
//       usr.selecttFun(client, sqlcount, function (result) {
//             sqlcounts = result[0].count;
//             var sqloneday = "SELECT count(DISTINCT Ip) count from web_log where dt>CURDATE();"
//             usr.selecttFun(client, sqloneday, function (result) {
//                   sqlonedays = result[0].count;
//                   var sqldetails = "SELECT count(DISTINCT Ip) count from web_log where operation='details';";
//                   usr.selecttFun(client, sqldetails, function (result) {
//                         sqldetailss = result[0].count;
//                         var sqlarticlecount = "SELECT count( DISTINCT articleId) count FROM web_log WHERE operation='details' AND articleId<>0 ";
//                         usr.selecttFun(client, sqlarticlecount, function (result) {
//                               sqlarticlecounts = result[0].count;
//                               res.end(JSON.stringify(jsonresult.JsonDatalist(sqlcounts, sqlonedays,sqldetailss,sqlarticlecounts)));
//                         })
//                   })

//             })
//       })
// })



//获取IP
function getClientIp(req) {
      var ipAddress;
      var forwardedIpsStr = req.header('x-forwarded-for');
      if (forwardedIpsStr) {
            var forwardedIps = forwardedIpsStr.split(',');
            ipAddress = forwardedIps[0];
      }
      if (!ipAddress) {
            ipAddress = req.connection.remoteAddress;
      }
      return ipAddress;
}


module.exports = router;