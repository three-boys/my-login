  var db=require('../Database/db').tag;
  var status = require('../status');

  var CRUD = {
  	read: function(query,callback){
  		db.find(query).toArray(function(err, items){ 
  			if(err){
  				return callback(status.fail);
  			}
  			var obj = {
  				status: status.success.status,
  				message: status.success.message,
  				items: items
  			};
  			return callback(obj);
  		});
  	}
  }
exports.CRUD = CRUD;