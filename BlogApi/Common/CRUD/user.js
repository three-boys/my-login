var db=require('../Database/db').user;
var status = require('../status');
var CRUD = {
	//增加
	 create: function(model, callback){
        db.save(model, function(err, item){
            if(err) {
                return callback(status.fail);
            }
            item.status = status.success.status;
            item.message = status.success.message;
            return callback(item);
        });
    },
    //删除
    delete: function(query, callback){
        db.remove(query, function(err){
              if(err){
                return callback(status.fail);
            }
            return callback(status.success);
        });
    },
    //修改
     update: function(query, updateModel, callback){
     	var set = updateModel;
        db.update(query, set, function(err){
                   if(err){
                return callback(status.fail);
            }else{
                return callback(status.success);
            }
        });
    },
	//查询
     read: function(query,pagenum,page,callback){

        db.find(query,{_id:0,itcode:1}).skip(pagenum*(page-1)).limit(pagenum).toArray(function(err, items){
 //db.find(query).toArray(function(err, items){
               db.count(function(err,count){  
            if(err){
                return callback(status.fail);
            }
            var obj = {
                status: status.success.status,
                message: status.success.message,
                items: items,
                counts:count
            };
            return callback(obj);
            }) 
        });
    },
};

exports.CRUD = CRUD;