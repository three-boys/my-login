var db=require('../Database/db').article;
var status = require('../status');

var CRUD = {
	//增加
	 create: function(model, callback){
        db.save(model, function(err, item){
            if(err) {
                return callback(status.fail);
            }
            item.status = status.success.status;
            item.message = status.success.message;
            return callback(item);
        });
    },
    //删除
    delete: function(query, callback){
        db.remove(query, function(err){
              if(err){
                return callback(status.fail);
            }
            return callback(status.success);
        });
    },
    //修改
     update: function(query, updateModel, callback){
     	var set = updateModel;
        db.update(query, set, function(err){
                   if(err){
                return callback(status.fail);
            }else{
                return callback(status.success);
            }
        });
    },
   
	//查询(带分页，条件)
     read: function(query,fields,pagesize,pageindex,callback){
        db.find(query,fields).skip(pagesize*(pageindex-1)).limit(parseInt(pagesize)).toArray(function(err, items){
            db.find(query,{"article":0}).toArray(function(err, num){
               // db.find(query,function(err,count){  
            if(err){
                return callback(status.fail);
            }
            var obj = {
                status: status.success.status,
                message: status.success.message,
                items: items,
                counts:num.length
            };
            return callback(obj);
            }) 
        });
    },
    readdetails: function(query,fields,callback){
            db.findOne(query,fields,function(err,items){
            if(err){
                return callback(status.fail);
            }
            var obj = {
                status: status.success.status,
                message: status.success.message,
                items: items
            };
            return callback(obj);
        });
    },
};

exports.CRUD = CRUD;
