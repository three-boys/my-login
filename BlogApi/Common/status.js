module.exports = {
    /*
    * 成功状态
    *
    * */
    success: {
        status: true,
        message: 'OK'
    },
    /*
    * 失败状态
    *
    * */
    fail: {
        status: false,
        message: 'FAIL'
    }
 };