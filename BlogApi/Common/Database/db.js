var settings  = require('../../config.json');
var mongo = require('mongoskin');
var s = settings;
var url = (s.user && s.pwd)
?'mongodb://' + s.user + ':' + s.pwd+'@' + s.host + '/' + s.db
:'mongodb://' + s.host + '/' + s.db;
var db = mongo.MongoClient.connect(url);


module.exports.user = db.collection('user', {strict: false});
module.exports.article = db.collection('article', {strict: false});
module.exports.tag = db.collection('tags', {strict: false});
module.exports.comments = db.collection('comments', {strict: false});