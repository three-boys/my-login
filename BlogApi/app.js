var express = require('express');
var status=require('./Common/status');
var bodyParser = require('body-parser')

//日志
var logs = require('./routes/logs');
//文章
var index = require('./routes/index');

var app = express();



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



// app.use(session({
//     secret: 'hubwiz app', //secret的值建议使用随机字符串
//     cookie: {maxAge: 60 * 1000 * 300} // 过期时间（毫秒）
// }));


//设置跨域访问
app.use(function (req, res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  res.setHeader("Content-Type", "application/json;charset:utf-8");
  return next();
});

//日志
app.use('/logs', logs);
//文章
app.use('/index', index);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
 res.end(JSON.stringify(status.fail));
});



app.listen(3000, function () {
    console.log("Star...");
})

module.exports = app;
