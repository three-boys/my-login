﻿var LanguageEN = {
    home: 'Home',
    login: 'Login',
    Posting:'Posting',
    exit:'Exit',
    newarticles:'Latest Articles',
    tags:'Tags'
}

var LanguageCN = {
    home: '首页',
    login: '登录',
    Posting:'发帖',
    exit:'退出',
    newarticles:'最新文章',
    tags:'标签'
    
}
var app = angular.module('language-app', []);
app.config(function ($translateProvider) {
    $translateProvider.translations('en', this.LanguageEN);
    $translateProvider.translations('cn', this.LanguageCN);
    //默认语言
    $translateProvider.preferredLanguage(localStorage.langusge == null ? "cn" : localStorage.langusge);
});