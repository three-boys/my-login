var app = angular.module('index-app', ['ngRoute', 'apps','language-app']);
app.config(['$routeProvider', function ($routeProvider) {
    //这里指定路由
    $routeProvider
        .when('/',
        {
            templateUrl: '/Tpl/index.tpl.html',
            controller: 'IndexController'
        })
        .when('/details/:Id', {
            url: 'detail/:Id',
            templateUrl: '/Tpl/details.tpl.html',
            controller: 'DetailsController'
        })
        .when('/searchs/', {
            url: 'search/',
            templateUrl: '/Tpl/selectResult.tpl.html',
            controller: 'SearchController'
        })
          .when('/posting/', {
            url: 'posting/',
            templateUrl: '/Tpl/posting.tpl.html',
            controller: 'PostingController'
        })
}]);

app.controller('PostingController',function(){
    
})

//获取文章
app.controller('IndexController', ['$http', '$scope', 'MsgService', 'ScrollTo', function ($http, $scope, MsgService, ScrollTo) {
    MsgService.insertlogs("visitors",0);
    $scope.ArticleList = new Array();
    var txtsearch='';
    $scope.pager = {
        page: 0, count: 0, pageSize: 8, busy: false, infinite_page: 2, infinite_disable: false,
        load_page: function (PageIndex, by_infinite,tag) {
           PageIndex= tag1=""?0:PageIndex;
            if (this.count > 0 && PageIndex > this.count) return;
            if (this.busy) return;
            this.busy = true;
            MsgService.getArticles(PageIndex, 8, tag,1)
                .success(function (data) {
                    if (!data || !data.items) return $scope.pager.busy = false;
                    if (!by_infinite || PageIndex > $scope.pager.infinite_page) {
                        if (!by_infinite) {
                            $scope.ArticleList = [];
                        }
                    }
                    if (data.items.length > 0) {
                        $scope.ArticleList = new Array();
                        for (var i = 0; i < data.items.length; i++) {
                            $scope.ArticleList.push(data.items[i]);
                        }
                    }else
                    {
                        $scope.ArticleList = new Array();
                    }
                    $scope.pager.page = PageIndex;
                    $scope.pager.count = Math.max(parseInt((data.counts + $scope.pager.pageSize - 1) / $scope.pager.pageSize), 1);



                    if ($scope.pager.pageSize >= data.counts) $scope.pager.infinite_disable = true;
                    else $scope.pager.infinite_disable = false;
                    if (!by_infinite) ScrollTo.idOrName();
                    $scope.pager.busy = false;
                });
        },
        infinite_load: function () {
            $scope.pager.load_page($scope.pager.page + 1, true,txtsearch);
        }
    };
    $scope.pager.infinite_load();


    $scope.$on('searchResult', function (event, data) {
        $scope.pager.page=0;
        txtsearch= data.name;
         $scope.pager.infinite_load();

    })
}])




app.controller('DetailsController', ['$http', '$scope', 'MsgService','$routeParams', 'ScrollTo', function ($http, $scope, MsgService,$routeParams, ScrollTo) {
    MsgService.insertlogs("visitors",0);
    $scope.commentList = new Array();
    $scope.pager = {
        page: 0, count: 0, pageSize: 2, busy: false, infinite_page: 2, infinite_disable: false,
        load_page: function (PageIndex, by_infinite) {
            if (this.count > 0 && PageIndex > this.count) return;
            if (this.busy) return;
            this.busy = true;
            MsgService.getComment(PageIndex, 2,$routeParams.Id)
                .success(function (data) {
                    if (!data || !data.items) return $scope.pager.busy = false;
                    if (!by_infinite || PageIndex > $scope.pager.infinite_page) {
                        if (!by_infinite) {
                            $scope.commentList = [];
                        }
                    }
                    if (data.items.length > 0) {
                        $scope.commentList = new Array();
                        for (var i = 0; i < data.items.length; i++) {
                            $scope.commentList.push(data.items[i]);
                        }
                    }else
                    {
                        $scope.commentList = new Array();
                    }
                    $scope.pager.page = PageIndex;
                    $scope.pager.count = Math.max(parseInt((data.counts + $scope.pager.pageSize - 1) / $scope.pager.pageSize), 1);



                    if ($scope.pager.pageSize >= data.counts) $scope.pager.infinite_disable = true;
                    else $scope.pager.infinite_disable = false;
                    if (!by_infinite) ScrollTo.idOrName();
                    $scope.pager.busy = false;
                });
        },
        infinite_load: function () {
            $scope.pager.load_page($scope.pager.page + 1, true);
        }
    };
    $scope.pager.infinite_load();
    $scope.$on('searchResult', function (event, data) {
        $scope.pager.page=0;
         $scope.pager.infinite_load();

    })
      //获取详细
    MsgService.getDetails($routeParams.Id).success(function (data) {
        $scope.ArticleDetails = data.items;
    })
    //提交留言
    $scope.submitMessage = function () {
        var obj = {
            articleId:$scope.ArticleDetails._id,
            contents: $scope.contents
        }
        MsgService.insertMessage(obj).success(function (result) {
            layer.alert(result.message)
        })
    }
    //点赞
    $scope.ding=function(id){
        MsgService.like(id).success(function(result){
            if(result.status)
            {
                $scope.ArticleDetails.like=$scope.ArticleDetails.like==undefined?1:$scope.ArticleDetails.like+1;
                layer.msg("感谢你的支持",{ icon: 1})
            }else{
                layer.msg("点击失败",{ icon: 2})
            }
        })
    }
    //不喜欢
        $scope.cai=function(id){
        MsgService.nolike(id).success(function(result){
            if(result.status)
            {
                $scope.ArticleDetails.nolike=$scope.ArticleDetails.nolike==undefined?1:$scope.ArticleDetails.nolike+1;
                layer.msg("我们会努力的",{ icon: 1})
            }else{
                layer.msg("点击失败",{ icon: 2})
            }
        })
    }
}])








//文章详细
app.controller('DetailsController1', ['$http', '$scope', '$routeParams', 'MsgService', function ($http, $scope, $routeParams, MsgService) {
    $scope.commentss = new Array();
    //获取详细
    MsgService.getDetails($routeParams.Id).success(function (data) {
        $scope.ArticleDetails = data.items;
    })
    //获取评论
    MsgService.getComment($routeParams.Id).success(function (data) {
        $scope.Maincomment = data.CData1;
        $scope.Childcomment = data.CData2;
    })

    //提交留言
    $scope.submitMessage = function () {
        var obj = {
            articleId:$scope.ArticleDetails._id,
            contents: $scope.contents
        }
        MsgService.insertMessage(obj).success(function (result) {
            layer.alert(result.message)
        })
    }
    //点赞
    $scope.ding=function(id){
        MsgService.like(id).success(function(result){
            if(result.status)
            {
                $scope.ArticleDetails.like=$scope.ArticleDetails.like==undefined?1:$scope.ArticleDetails.like+1;
                layer.msg("感谢你的支持",{ icon: 1})
            }else{
                layer.msg("点击失败",{ icon: 2})
            }
        })
    }
    //不喜欢
        $scope.cai=function(id){
        MsgService.nolike(id).success(function(result){
            if(result.status)
            {
                $scope.ArticleDetails.nolike=$scope.ArticleDetails.nolike==undefined?1:$scope.ArticleDetails.nolike+1;
                layer.msg("我们会努力的",{ icon: 1})
            }else{
                layer.msg("点击失败",{ icon: 2})
            }
        })
    }
      

    $scope.praise = function (AId, CId) {
        var obj = {
            AId: AId,
            CId: CId == undefined ? 0 : CId,
        }
        MsgService.insertPraise(obj).success(function (result) {
            layer.alert(result.message)
        })
    }

}])

//查询结果
app.controller('SearchController', ['$http', '$scope', '$routeParams', 'MsgService', 'ScrollTo', function ($http, $scope, $routeParams, MsgService, ScrollTo) {
    $scope.selectArticleList = new Array();
    $scope.aa = $routeParams.value;
    $scope.pager = {
        page: 0, count: 0, pageSize: 8, busy: false, infinite_page: 2, infinite_disable: false,
        load_page: function (PageIndex, by_infinite) {
            if (this.count > 0 && PageIndex > this.count) return;
            if (this.busy) return;
            this.busy = true;
            MsgService.getArticles(PageIndex, 8, $routeParams.value,2)
                .success(function (data) {
                    if (!data || !data.items) return $scope.pager.busy = false;
                    if (!by_infinite || PageIndex > $scope.pager.infinite_page) {
                        if (!by_infinite) {
                            $scope.selectArticleList = [];
                        }
                    }
                    if (data.items.length > 0) {
                        $scope.selectArticleList = new Array();
                        for (var i = 0; i < data.items.length; i++) {
                            $scope.selectArticleList.push(data.items[i]);
                        }
                    }
                    $scope.pager.page = PageIndex;
                    $scope.pager.count = Math.max(parseInt((data.counts + $scope.pager.pageSize - 1) / $scope.pager.pageSize), 1);



                    if ($scope.pager.pageSize >= data.counts) $scope.pager.infinite_disable = true;
                    else $scope.pager.infinite_disable = false;
                    if (!by_infinite) ScrollTo.idOrName();
                    $scope.pager.busy = false;
                });
        },
        infinite_load: function () {
            $scope.pager.load_page($scope.pager.page + 1, true);
        }
    };
    $scope.pager.infinite_load();
}]);


app.directive('stringHtml' , function(){
  return function(scope , el , attr){
    if(attr.stringHtml){
      scope.$watch(attr.stringHtml , function(html){
        el.html(html || '');//更新html内容
      });
    }
  };
});


