﻿var apps = angular.module('apps', ['pascalprecht.translate', 'language-app'])
//头部
apps.directive("headers", function () {
    return {
        priority: 99999,
        restrict: 'AE',
        templateUrl: '/Tpl/header.tpl.html',
        replace: true, //放在template后面才会生效
        controller: function ($scope, $rootScope, $http, $element, $translate) {
            $scope.language = function (langkey) {
                localStorage.langusge = langkey;
                //转换语言版本
                $translate.use(langkey);
            }
        }
    }
})

//搜索
apps.directive("navs", function () {
    return {
        priority: 99999,
        restrict: 'AE',
        templateUrl: '/Tpl/search.tpl.html',
        replace: true, //放在template后面才会生效
        controller: function ($scope, $rootScope, $http, $element, $translate) {
            $scope.search = function () {
                location.href = "#/searchs/?value=" + $scope.searchs;
            }
        }
    }
})

//右侧
apps.directive("rights", function () {
    return {
        priority: 99999,
        restrict: 'AE',
        templateUrl: '/Tpl/right.tpl.html',
        replace: true, //放在template后面才会生效
        controller: function ($scope, $rootScope, $http, $element, $translate, MsgService) {

            $scope.TagList = new Array();

            //访问统计
            MsgService.getlogs().success(function (result) {
                $scope.statistical = result;
            })



            $http.get('http://localhost:3000/index/getNewArticles').success(function (datas) {
                $scope.ArticleList = datas;
            })

            $http.get('http://localhost:3000/index/getTags').success(function (datas) {

                $scope.TagList = fn(datas.items, 0);
            })

            $scope.selecttag = function (tagname) {
                $scope.$broadcast('searchResult', { name: tagname }) //监听
            }
        }
    }
})

//底部
apps.directive("footers", function () {
    return {
        priority: 99999,
        restrict: 'AE',
        templateUrl: '/Tpl/footer.tpl.html',
        replace: true, //放在template后面才会生效
        controller: function ($scope, $rootScope, $http, $element, $translate) {

        }
    }
})



//-----------------------公用------------------------------------
/*分页器*/
apps.directive('pagers', function () {
    return {
        priority: 9999999,
        restrict: 'AE',
        template:
        '<div class="pagerbox"><nav><ul class="pagination">' +
        '<li ng-repeat="p in pages" style="list-style:none;float:left" ng-class="{disabled:p.disabled}" ng-click="p.href?pager.load_page(p.href):0" ><a href="javascript:void(0)" class="{{p.css}}" ng-class="{active:p.active}">{{p.display}}</a></li>' +
        '</ul></nav></div>',
        replace: true, //放在template后面才会生效
        scope: { pager: '=' },
        controller: function ($scope, $http, $element) {

            /*
                  分页逻辑：
                  1:小于等于10页，全部展示
                  2：大于10页 （初次）
                      1：前5页，后5页 ，中间省略号
                  3：点击状态
                    1：显示第一页 省略号 中间5页 省略号 最后一页
            */

            $scope.$watch('pager.page', function (newValue, oldValue) {
                //if ($scope.pages && newValue == newValue) return;
                var $pager = $scope.pager;
                var page_current = newValue;
                var page_count = $scope.pager ? $scope.pager.count : 1;
                var limit = 5;
                $scope.pages = [];
                $scope.pages.push({ label: 'Previous', display: '上一页', href: Math.max(1, parseInt(page_current) - 1), css: 'prevPage', disabled: page_current == 1 });

                if ($scope.pager && $scope.pager.count <= 10) {
                    for (var i = 1; i <= $scope.pager.count; i++) {
                        $scope.pages.push({ display: i, href: i, css: 'pagelist', active: page_current == i });
                    }
                } else {
                    if (page_current < limit) {
                        //页码：1,2,3,4
                        for (var i = 1; i <= limit; i++) {
                            $scope.pages.push({ display: i, href: i, css: 'pagelist', active: page_current == i });
                        }
                        $scope.pages.push({ display: '...', href: null, css: 'pagelist', disabled: true });
                        $scope.pages.push({ display: page_count, href: page_count, css: 'pagelist' });

                    } else if (page_current > page_count - limit) {
                        //页码：n,n-1,n-2,n-3,n-4
                        $scope.pages.push({ display: 1, href: 1, css: 'pagelist' });
                        $scope.pages.push({ display: '...', href: null, css: 'pagelist', disabled: true });
                        for (var i = page_count - limit; i <= page_count; i++) {
                            $scope.pages.push({ display: i, href: i, css: 'pagelist', active: page_current == i });
                        }

                    } else {
                        //页码位于中间部分
                        $scope.pages.push({ display: 1, href: 1, css: 'pagelist' });
                        $scope.pages.push({ display: '...', href: null, css: 'pagelist', disabled: true });
                        for (var i = parseInt(page_current) - 2; i <= parseInt(page_current) + 2; i++) {
                            $scope.pages.push({ display: i, href: i, css: 'pagelist', active: page_current == i });
                        }
                        $scope.pages.push({ display: '...', href: null, css: 'pagelist', disabled: true });
                        $scope.pages.push({ display: page_count, href: page_count, css: 'pagelist' });
                    }
                }
                $scope.pages.push({ label: 'Next', display: '下一页', href: Math.min(page_count, parseInt(page_current) + 1), css: 'nextPage', disabled: page_current == page_count });
                // //加载动画
                if ($pager.busy) $(".loading-container").removeClass("loading-inactive");
                else $(".loading-container").addClass("loading-inactive");
            });
        }
    }
});

/*页面滚动(修改了开源的scrollTo，因为有浏览器兼容性问题)*/
apps.directive('scrollTo', ['ScrollTo', function (ScrollTo) {

    return {
        restrict: "AC",
        compile: function () {

            return function (scope, element, attr) {
                element.bind("click", function (event) {
                    event.preventDefault();
                    ScrollTo.idOrName(attr.scrollTo, attr.offset);
                });

                //控制【回到顶部的标签】的显示或隐藏
                window.onscroll = function () {
                    element.css('display', (document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop >= 300) ? "block" : "none");
                }
            };
        }
    };
}]);


apps.provider("ngScrollToOptions", function () {
    this.options = {
        handler: function (el, offset) {
            if (offset) {
                var top = $(el).offset().top - offset;
                window.scrollTo(0, top);
            }
            else {
                el.scrollIntoView();
            }
        }
    };
    this.$get = function () {
        return this.options;
    };
    this.extend = function (options) {
        this.options = angular.extend(this.options, options);
    };
});

/*字符串截取+省略号  {{item.ArticleTitle |Cut:0 :17 }}*/
apps.filter("Cut", function ($filter) {
    return function (input, star, end) {
        if (input == null) { return ""; }
        if (input.length > end) {
            return input.substring(star, end) + "...";
        }
        return input;
    }
});

/*c#日期格式 转为javascript的日期格式*/
apps.filter('datetime', function ($filter) {
    return function (input) {
        if (input == null || input==undefined) return "";
        var da = eval('new ' + input.replace('/', '', 'g').replace('/', '', 'g'));
        var date = new Date(da);
        return date;
    };
});

/*高亮显示*/
apps.filter("highlight", function ($filter, $sce) {
    return function (input, p) {
        if (input) {
            // input = input.replace(p, "<span class='keytext'>" + p + "</span>", 'g');
            input = input.replace(p, "<span style='color:red;font-size:20px'>" + p + "</span>", 'g');
        }
        return $sce.trustAsHtml(input);
    }
});

/*处理发表的说说时间（刚刚、几天、显示数字）*/
apps.filter('dateformat', function ($filter, datetimeService) {
    return function (dateinfo) {
        if (dateinfo == '' || dateinfo==undefined) { return ""; }
        if (dateinfo != '') {
            var d=datetimeService.sjczh(dateinfo)
            var data = datetimeService.ChangeDateFormat(d);
            return datetimeService.GetDateDiff(data);
        } else {
            return dateinfo;
        }
    };
});

/*处理时间相关的函数*/
apps.service('datetimeService', function () {
    var result = "";

    this.sjczh=function(dateTime)
    {
        var date = new Date(parseInt(dateTime));
        Y = date.getFullYear() + '-';
        M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        D = date.getDate() + ' ';
        h = date.getHours() + ':';
        m = date.getMinutes() + ':';
        s = date.getSeconds(); 
        return Y+M+D+h+m+s;
    }
    this.ChangeDateFormat = function (dateTime) {
        var date = new Date(dateTime.replace("/Date(", ""));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        // var sec = date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds();
        //return date.getFullYear() + "-" + month + "-" + currentDate + " " +hour+":"+min+":"+sec;
        //  return date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + min;
        var temp = date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + min;
        return temp;
    };

    this.GetDateDiff = function (dateTime) {
        var sec = 1000;
        var minute = 1000 * 60;
        var hour = minute * 60;
        var day = hour * 24;
        var month = day * 30;

        var tempTime = dateTime ? Date.parse(dateTime.replace(/-/gi, "/")) : new Date().getTime();
        var now = new Date().getTime();
        var diffValue = now - tempTime;

        var monthTemp = diffValue / month;
        var weekTemp = diffValue / (7 * day);
        var dayTemp = diffValue / day;
        var hourTemp = diffValue / hour;
        var minTemp = diffValue / minute;
        var secTemp = diffValue / sec;

        if (monthTemp >= 1) {
            return dateTime;
        }
        else if (weekTemp >= 1) {
            result = "" + parseInt(weekTemp) + " 星期前";
            // return dateTime;
        }
        else if (dayTemp >= 1) {
            result = "" + parseInt(dayTemp) + " 天前";
            //return dateTime;
        }
        else if (hourTemp >= 1) {
            result = "" + parseInt(hourTemp) + " 小时前";
        }
        else if (minTemp >= 1) {
            result = "" + parseInt(minTemp) + " 分钟前";
        }
        else if (secTemp >= 1) {
            result = "" + parseInt(secTemp) + " 秒钟前";
        }
        else
            result = "刚刚";
        return result;
    };

    this.ChangeDateFormat4New = function (dateTime) {
        var date = new Date(parseInt(dateTime.replace("/Date(", "").replace(")/", ""), 10));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        //var sec = date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds();
        //return date.getFullYear() + "-" + month + "-" + currentDate + " " +hour+":"+min+":"+sec;
        return date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + min;
    };

    this.GetDateDiff4New = function (dateTime) {
        var sec = 1000;
        var minute = 1000 * 60;
        var hour = minute * 60;
        var day = hour * 24;
        var month = day * 30;

        var tempTime = dateTime ? Date.parse(dateTime.replace(/-/gi, "/")) : new Date().getTime();

        var now = new Date().getTime();
        var diffValue = now - tempTime;

        var monthTemp = diffValue / month;
        var weekTemp = diffValue / (7 * day);
        var dayTemp = diffValue / day;
        var hourTemp = diffValue / hour;
        var minTemp = diffValue / minute;
        var secTemp = diffValue / sec;

        if (monthTemp >= 1) {
            return dateTime.substring(0, 10);
        }
        else if (weekTemp >= 1) {
            //result = "" + parseInt(weekTemp) + " 星期前";
            return dateTime.substring(0, 10);
        }
        else if (dayTemp >= 1) {
            //result = "" + parseInt(dayTemp) + " 天前";
            return dateTime.substring(0, 10);
        }
        else if (hourTemp >= 1) {
            result = "" + parseInt(hourTemp) + " 小时前";
        }
        else if (minTemp >= 1) {
            result = "" + parseInt(minTemp) + " 分钟前";
        }
        else if (secTemp >= 10) {
            result = "" + parseInt(secTemp) + " 秒钟前";
        }
        else
            result = "刚刚";
        return result;
    };

});

//辅助分页
apps.service('ScrollTo', ['$window', 'ngScrollToOptions', function ($window, ngScrollToOptions) {

    this.idOrName = function (idOrName, offset, focus) {//find element with the given id or name and scroll to the first element it finds
        var document = $window.document;

        if (!idOrName) {//move to top if idOrName is not provided
            $window.scrollTo(0, 0);
            return;
        }

        if (focus === undefined) { //set default action to focus element
            focus = true;
        }

        //check if an element can be found with id attribute
        var el = document.getElementById(idOrName);
        if (!el) {//check if an element can be found with name attribute if there is no such id
            el = document.getElementsByName(idOrName);

            if (el && el.length)
                el = el[0];
            else
                el = null;
        }

        if (el) { //if an element is found, scroll to the element
            if (focus) {
                el.focus();
            }

            ngScrollToOptions.handler(el, offset);
        }

        //otherwise, ignore
    }

}]);


//------------------------------其他服务-----------------------------------------
apps.service('MsgService', function ($http) {
    this.getArticles = function (pageIndex, pageSize, txtsearch, st) {
        var obj = {
            pageIndex: pageIndex,
            pageSize: pageSize,
            txtsearch: txtsearch,
            st: st
        };
        return $http({
            method: 'get',
            url: 'http://localhost:3000/index/getArticles',
            params: obj
        })
    }

    this.getComment = function (pageindex,pagesize,Id) {
        var obj={
            pageSize:pagesize,
            pageIndex:pageindex,
            Id:Id
        };
        return $http({
            method: 'get',
            url: 'http://localhost:3000/index/getComment',
            params: obj
        })
    }
    this.getDetails = function (Id) {
        return $http({
            method: 'get',
            url: 'http://localhost:3000/index/getDetails',
            params: { Id: Id }
        })
 //        var k="value=";
 //        var k=k+JSON.stringify(obj);
 // return $http.post('http://localhost:3000/index/getDetails', k, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
    
    }

    this.insertMessage = function (obj) {
        return $http.post('http://localhost:3000/index/insertMessage', JSON.stringify(obj), { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
    }
    this.insertPraise = function (obj) {
        return $http.post('http://localhost:3000/index/insertPraise', JSON.stringify(obj), { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
    }

    //插入日志
        this.insertlogs = function (operation, articleId) {
            return $http({
                method: 'get',
                url: 'http://localhost:3000/logs/insertlogs',
                params: { operation: operation, articleId: articleId }
            })
        }
 
    //获取日志信息
    this.getlogs = function (operation, articleId) {
        return $http({
            method: 'get',
            url: 'http://localhost:3000/logs/getlogs',
        })
    }

    //点赞
    this.like=function(id){
        return $http({
            method:'get',
            url:'http://localhost:3000/index/like',
            params:{Id:id}
        });
    }
     //不喜欢
    this.nolike=function(id){
        return $http({
            method:'get',
            url:'http://localhost:3000/index/nolike',
            params:{Id:id}
        });
    }
});


//递归实现右侧菜单
function fn(data, pid) {

    var result = [];
    var temp;
    for (var i = 0; i < data.length; i++) {

        if (data[i].Oid == pid) {
            var obj = { "Name": data[i].Name, "Id": data[i]._id };
            temp = fn(data, data[i]._id);
            if (temp.length > 0) {
                obj.children = temp;
            }
            result.push(obj);
        }
    }
    return result;
}