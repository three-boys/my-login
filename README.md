## 说明文档

# 配置源代码管理说明
* 0.第一次获取 git clone git@gitlab.com:three-boys/my-login.git
* 1.进入项目文件夹(说白了这个文件夹里有一个.git的隐藏文件夹，这里面记载着项目信息) cd my-login
* 2.查看修改状态 git status
* 3.提交到本地仓储（提交代码第一步）  git add .
* 4.写提交的注释（提交代码第二步）  git commit -m '你修改内容的描述'
* 5.提交到远程的源代码管理器（提交代码第三步） git push
* 6.非第一次获取最新代码  git pull 

# 项目描述
* 本仓储包括博客的项目前台、后台的源代码和Mysql的脚本
* 前台主要技术angularjs
* 后台主要技术nodejs

# 项目的本地搭建
*
 